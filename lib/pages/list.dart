import 'package:coffee_order/pages/detail.dart';
import 'package:flutter/material.dart';

class ListPage extends StatefulWidget {
  const ListPage({Key? key}) : super(key: key);

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const DetailPage(title: 'Kopi Susu'),
              ),
            );
          },
          child: Container(
            margin: const EdgeInsets.all(24.0),
            decoration: const BoxDecoration(
              color: Colors.orange,
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset(
                    'asset/img/kopisusu.jpg',
                    height: 300,
                  ),
                ),
                const Text('Kopi Susu'),
                const Text('IDR 17.000'),
                const SizedBox(
                  height: 12,
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}
