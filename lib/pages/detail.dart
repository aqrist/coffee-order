import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Column(
        children: [
          Image.asset('asset/img/kopisusu.jpg'),
          const Padding(
            padding: EdgeInsets.all(12.0),
            child: Text(
                'This package contains a set of high-level functions and classes that make it easy to consume HTTP resources. Its multi-platform, and supports mobile, desktop, and the browser'),
          )
        ],
      ),
    );
  }
}
