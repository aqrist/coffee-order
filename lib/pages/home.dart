import 'package:coffee_order/pages/list.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Coffee Order'),
          bottom: const TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.coffee),
                text: 'Cofeee',
              ),
              Tab(
                icon: Icon(Icons.coffee),
                text: 'non Coffee',
              ),
            ],
          ),
        ),
        body: const TabBarView(
          children: [
            ListPage(),
            ListPage(),
          ],
        ),
      ),
    );
  }
}
